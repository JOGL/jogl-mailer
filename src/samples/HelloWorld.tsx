// Npm modules
import { Mjml, MjmlBody, MjmlColumn, MjmlSection, MjmlText } from "mjml-react";
import React from "react";

export const NewPost: React.FC = () => {
  return (
    <Mjml>
      <MjmlBody>
        <MjmlSection>
          <MjmlColumn>
            <MjmlText>Hello World!</MjmlText>
          </MjmlColumn>
        </MjmlSection>
      </MjmlBody>
    </Mjml>
  );
};
