// Npm modules
import { render } from "mjml-react";
import React from "react";
// Internal modules
import { NewPost } from "./samples";

console.log("JOGL Mailer");

// Compile an mjml template
const { html } = render(<NewPost></NewPost>, { validationLevel: "soft" });

console.log(html);
