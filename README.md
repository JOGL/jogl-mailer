# jogl-mailer

A node mailer using react-mjml to produced beautiful emails for the JOGL platform.

## Getting Started

- Run `yarn build` to compile.

- Run `yarn start` to produce sample output.
